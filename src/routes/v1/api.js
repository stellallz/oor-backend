const express = require('express');
const router = new express.Router();
const usersControllers = require('../../controllers/api/v1/users');




router.get('/users', usersControllers.index);
router.get('/users/:id', usersControllers.show)
router.put('/users/:id', usersControllers.update);
router.post('/users', usersControllers.store);
router.delete('/users/:id', usersControllers.destroy);



 module.exports = router;