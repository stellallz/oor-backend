const User = require('../../../models/user');
// PUT one user
exports.update = async (req, res) => {
  
};

// DELETE one user
exports.destroy = async (req, res) => {
   
};

// POST/create one user
exports.store = async (req, res) => {
  const user = new User(req.body);

    try {
        await user.save();
        //const token = await user.generateAuthToken();
        res.status(201).send({user /* token */});
    } catch (e) {
        res.status(400).send(e);

           
    }
};

// GET one user
exports.show = async (req, res) => {
  
};

// GET all users
exports.index = async (req, res) => {
  return res.status(200).send("TEST");
};


  
